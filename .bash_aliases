# .bashrc

# User specific aliases and functions

alias cook='cd /home/student/Cyber/'
#alias set='cd /home/student/Cyber/setup/'
alias go='pushd . ; cook ; cd setup ; ./run.sh ; popd'
alias rm='rm -i'
alias cp='cp -i'
alias mv='mv -i'
alias nb='/bin/new_bash.sh' # used to make new bash files
alias nr='/bin/new_ruby.sh' # used to make new ruby files
alias np='/bin/new_python.sh' # used to make new python files
alias refresh='source ~/.bashrc'
alias com='cat /etc/motd'
# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi




