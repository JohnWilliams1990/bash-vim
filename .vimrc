


au VimLeave * :!clear
" set the comment character to follow cariage retruns
set fo+=r
set nohlsearch
colorscheme darkblue
set number
cabbrev Wq wq
cabbrev WQ wq


map <F3>  :put!="
map <F4>  :put="
map <F6>  :tabe 
map <F7>  :tabp<CR>
map <F8>  :tabn<CR>
map <F9>  :set nonumber <CR>
map <F10> :set number<CR>
map <F11> :set nohlsearch <CR>
map <F12> :set hlsearch <CR>





